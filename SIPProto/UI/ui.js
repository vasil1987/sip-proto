﻿angular.module("app", ["ui.bootstrap"]).
controller("main", function ($scope, $http, $log, $uibModal) {
    var connection = $.connection('/foo');
    connection.start(function () {
        $log.debug("пущено");
    });

    $scope.logs = [];

    connection.received(function (data) {
        if (data.type === "log") {
            $scope.$apply(function () {
                $scope.logs.splice(0, 0, data);
            });

            //$scope.logs.push(data);
        };

        if (data.type === "call") {
            $scope.incall(data.id, data.phone);
        };
        $log.debug(data.name);
    });

    $scope.call = function (phone) {
        $http.put("call/" + phone);
    };

    $scope.users = [];
    $scope.update = function () {
        $http.get("users").then(function (response) {
            $scope.users = response.data;
        });
    };

    $scope.update();

    $scope.incall = function (id, caller) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: "tpls/call.html",
            controller: "call",
            size: "sm",
            resolve: {
                details: function () {
                    return {
                        id: id,
                        caller: caller
                    };
                }
            }
        });

        modalInstance.result.then(function (remote) {
        }, function () {
        });

    };

    $scope.editUsers = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'tpls/users.html',
            controller: 'users',
            size: "lg"
        });

        modalInstance.result.then(function (remote) {
        }, function () {
            $scope.update();
        });

    };
}).controller("users", function ($scope, $uibModalInstance, $http) {

    $scope.users = [];

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    }

    $scope.close = function () {
        $uibModalInstance.close();
    }

    $scope.update = function () {
        $http.get("users").then(function (response) {
            $scope.users = response.data;
        });
    };

    $scope.update();

    $scope.addUser = function (name, phone) {
        $http.post("user", { name: name, phone: phone }).then(function () {
            $scope.update();
        });
    };

    $scope.deleteUser = function (user) {
        $http.delete(user.Id).then(function () {
            $scope.update();
        });
    };
}).controller("call", function ($scope, $http, $uibModalInstance, details) {

    $scope.details = details;

    $scope.close = function () {
        $http.put("answer/" + $scope.details.id + "/false").then(function (response) {
            $uibModalInstance.close();
        });       
    }

    $scope.accept = function () {
        $http.put("answer/" + $scope.details.id + "/true").then(function (response) {
            $uibModalInstance.close();
        });        
    }
});