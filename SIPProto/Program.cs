﻿using Microsoft.Practices.Unity;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using Topshelf;

namespace SIPProto
{
    class Program
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {                        
            HostFactory.Run(x =>
            {
                //x.UseNLog();

                x.Service<Service>(s =>
                {
                    s.ConstructUsing(settings => new Service());
                    s.WhenStarted(service => service.Start());
                    s.WhenStopped(service => service.Stop());                    
                });
                x.StartAutomatically();
                x.SetServiceName("sipproto");
                x.RunAsNetworkService();
            });
        }
    }
}
