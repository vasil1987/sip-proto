﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPProto
{
    class Call
    {
        public string Id { get; set; }
        public DateTime Date { get; set; }
        public string Phone { get; set; }

        /// <summary>
        /// in/out
        /// </summary>
        public string Direction { get; set; }
    }
}
