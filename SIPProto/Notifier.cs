﻿using Microsoft.AspNet.SignalR;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPProto
{
    class Notifier : Hub
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public void Call()
        {

        }

        public void Bar(string msg)
        {
            logger.Debug("поступило {0}", msg);
        }
    }
}
