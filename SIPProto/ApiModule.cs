﻿using Nancy;
using Newtonsoft.Json;
using Raven.Client.Embedded;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPProto
{
    public class ApiModule : NancyModule
    {
        public ApiModule(EmbeddableDocumentStore store, Caller caller)
        {
            Get[""] = (_) =>
            {
                return Response.AsFile(@"ui/index.html");
            };

            Get["users"] = (_) =>
            {
                using (var session = store.OpenSession())
                {
                    var users = session.Query<User>();
                    return Json(users);
                }
            };

            Post["user"] = (_) =>
            {
                var user = this.GetRequestObject<User>();
                using (var session = store.OpenSession())
                {
                    session.Store(user);
                    session.SaveChanges();
                }
                return "";
            };

            Delete["users/{id}"] = (arg) =>
            {
                using (var session = store.OpenSession())
                {
                    session.Delete(string.Format("users/{0}", arg.id));
                    session.SaveChanges();
                }
                return "";
            };

            Put["call/{phone}"] = (arg) =>
            {
                caller.Call(arg.phone);
                return "";
            };

            Put["answer/{id:int}/{accept:bool}"] = (arg) =>
            {
                caller.Answer(arg.id, arg.accept);
                return "";
            };
        }

        private Response Json(dynamic obj)
        {
            var str = JsonConvert.SerializeObject((object)obj);
            var bytes = Encoding.UTF8.GetBytes(str);
            return new Response
            {
                ContentType = "application/json",
                Contents = s => s.Write(bytes, 0, bytes.Length)
            };
        }
    }

    static class NancyPostExtensions
    {
        public static dynamic GetBody(this Request request)
        {
            var len = (int)request.Body.Length;
            var buffer = new byte[len];
            request.Body.Read(buffer, 0, len);
            var json = Encoding.UTF8.GetString(buffer);
            return JsonConvert.DeserializeObject<ExpandoObject>(json);
        }

        public static T GetRequestObject<T>(this NancyModule module)
        {
            var len = (int)module.Request.Body.Length;
            var buffer = new byte[len];
            module.Request.Body.Read(buffer, 0, len);
            var str = Encoding.UTF8.GetString(buffer);
            return JsonConvert.DeserializeObject<T>(str);
        }

    }
}
