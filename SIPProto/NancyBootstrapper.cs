﻿using Microsoft.Practices.Unity;
using Nancy.Bootstrappers.Unity;
using Nancy.Conventions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPProto
{
    class NancyBootstrapper : UnityNancyBootstrapper
    {
        private IUnityContainer unityContainer;

        public NancyBootstrapper(IUnityContainer unityContainer)
        {
            this.unityContainer = unityContainer;
        }

        protected override IUnityContainer GetApplicationContainer()
        {
            return unityContainer;
        }

        protected override void ConfigureConventions(Nancy.Conventions.NancyConventions nancyConventions)
        {
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory("ui", @"ui"));
        }
    }
}
