﻿using Microsoft.Owin.Hosting;
using Microsoft.Practices.Unity;
using NLog;
using Owin;
using Raven.Client.Embedded;
using System;
using System.Configuration;
using System.Dynamic;
using System.Threading;
using System.Threading.Tasks;

namespace SIPProto
{
    public class Service
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private IDisposable host;
        private Caller caller;

        public Service()
        {
        }

        public async Task Start()
        {
            try
            {
                //new Timer((a) => {
                //    dynamic msg = new ExpandoObject();
                //    msg.type = "log";
                //    msg.date = DateTime.Now;
                //    msg.name = "hello";
                //    msg.message = "привет!";
                //    FooConn.IncomingCall(msg);
                //}).Change(0, 3000);

                var uc = new UnityContainer();

                caller = new Caller();
                uc.RegisterInstance(caller);


                var store = new EmbeddableDocumentStore
                {
                    DataDirectory = "Data",
                    UseEmbeddedHttpServer = false
                };
                store.Initialize();
                uc.RegisterInstance(store);

                var url = ConfigurationManager.AppSettings["web-url"];
                host = WebApp.Start(url, app =>
                {
                    app.MapSignalR<FooConn>("/foo");
                    app.UseNancy(n => n.Bootstrapper = new NancyBootstrapper(uc));
                });

                logger.Info("сервис запущен, url: {0}", url);

                await caller.Start();
            }catch(Exception ex)
            {
                logger.Error(ex, "что-то пошло не так");
            }
        }

        public void Stop()
        {
            caller.Stop();
            logger.Info("сервис остановлен");
        }
    }
}
