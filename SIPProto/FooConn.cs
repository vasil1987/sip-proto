﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPProto
{
    class FooConn : PersistentConnection
    {
        public static void IncomingCall(string phone)
        {
            dynamic msg = new ExpandoObject();
            msg.date = DateTime.Now;
            msg.type = "call";
            msg.phone = phone;
            GlobalHost.ConnectionManager.GetConnectionContext<FooConn>().Connection.Broadcast((object)msg);
        }

        public static void Log(string message)
        {
            dynamic msg = new ExpandoObject();
            msg.date = DateTime.Now;
            msg.type = "log";
            msg.id = Guid.NewGuid();
            msg.message = message;
            GlobalHost.ConnectionManager.GetConnectionContext<FooConn>().Connection.Broadcast((object)msg);
        }

        public static void IncomingCall(string destinationUri, int id)
        {
            dynamic msg = new ExpandoObject();
            msg.date = DateTime.Now;
            msg.id = id;
            msg.type = "call";            
            msg.phone = destinationUri;
            GlobalHost.ConnectionManager.GetConnectionContext<FooConn>().Connection.Broadcast((object)msg);
        }
    }
}
