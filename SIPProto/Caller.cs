﻿using NLog;
using pjsip4net.Configuration;
using pjsip4net.Core.Configuration;
using pjsip4net.Interfaces;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SIPProto
{
    public class Caller
    {
        private ISipUserAgent ua;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public async Task Start()
        {
            await Task.Run(() =>
             {
                 try
                 {
                     //new System.Threading.Timer((_) => {
                     //    logger.Debug("было");
                     //    FooConn.IncomingCall("hahaha", 42);
                     //}).Change(5000, 15000);

                     var cfg = Configure.Pjsip4Net().FromConfig();

                     ua = cfg.Build().
                         Start();

                     ua.CallManager.IncomingCall += (se, ea) =>
                     {
                         FooConn.Log("событие IncomingCall ");

                         var call = ua.CallManager.GetCallById(ea.Data.Id);
                         FooConn.Log("звонок с id=" + call.Id);
                         FooConn.Log("звонок от=" + ea.Data.RemoteContact);
                         FooConn.IncomingCall(ea.Data.RemoteContact, call.Id);
                         FooConn.Log("отвечаем на звонок");
                         call.Answer(true);

                         //ea.Data
                         //logger.Debug("входящий звонок");
                         //FooConn.Log("входящий звонок от " + ea.Data.RemoteContact);

                     };

                     ua.Log += (se, ea) =>
                     {
                         //FooConn.Log(ea.Data);
                         //logger.Debug(ea.Data);
                     };

                     ua.CallManager.CallTransfer += (se, ea) =>
                     {
                         FooConn.Log("событие CallTransfer ");
                         //ea.Destination
                     };

                     ua.CallManager.CallStateChanged += (se, ea) =>
                     {
                         FooConn.Log("событие CallStateChanged ");
                         //try
                         //{
                         //    var call = (se as ICallManager).GetCallById(ea.Id);
                         //    if (call == null)
                         //    {
                         //        FooConn.Log("не найден звонок с id=" + ea.Id);
                         //        return;
                         //    }

                         //    if (call.IsIncoming)
                         //    {
                         //        FooConn.Log("входящий звонок от " + call.DestinationUri);
                         //        FooConn.IncomingCall(call.DestinationUri, call.Id);
                         //    }

                         //    if (call.IsActive)
                         //    {
                         //        FooConn.Log("играем wav");
                         //        var player = cfg.Container.Get<IWavPlayer>();
                         //        if (player == null)
                         //        {
                         //            FooConn.Log("не удалось создать плеер");
                         //            return;
                         //        }
                         //        player.Start("sample.wav", false);
                         //        player.Completed += (sse, eea) =>
                         //        {
                         //            FooConn.Log("бросаем трубку");
                         //            call.Hangup();
                         //            player.Dispose();
                         //        };
                         //        ua.MediaManager.ConferenceBridge.ConnectToSoundDevice(player.ConferenceSlot.SlotId);
                         //    }

                         //    FooConn.Log("состояние изменилось");
                         //    logger.Debug("DestinationUri={0}; Duration={1}; Id={2}", ea.DestinationUri, ea.Duration, ea.Id);
                         //}
                         //catch (Exception ex)
                         //{
                         //    logger.Error(ex, "смена статуса");
                         //}
                     };

                     ua.CallManager.Ring += (se, ea) =>
                     {
                         FooConn.Log("событие Ring ");
                         logger.Debug("ring callid={0}; IsRingback={1}; RingOn={2}", ea.CallId, ea.IsRingback, ea.RingOn);
                     };

                     FooConn.Log("SIP клиент запущен");
                 }
                 catch (Exception ex)
                 {
                     FooConn.Log("ошибка запуске " + ex.Message);
                 }
             });
        }

        public void Answer(int id, bool accept)
        {
            logger.Debug("попытка ответить на звонок {0} {1}", id, accept);
            try
            {
                if (accept)
                    ua.CallManager.GetCallById(id).Answer(accept);
                else
                    ua.CallManager.GetCallById(id).Hangup();
            }
            catch (Exception ex)
            {
                FooConn.Log("ошибка при ответе на звонок " + ex.Message);
            }
        }

        public void Stop()
        {
            try
            {
                ua.Dispose();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "останов");
            }
        }

        public void Call(string phone)
        {
            try
            {
                var call = ua.CallManager.MakeCall(phone);
            }
            catch (Exception ex)
            {
                FooConn.Log("ошибка при звонке " + ex.Message);
            }
        }
    }
}
